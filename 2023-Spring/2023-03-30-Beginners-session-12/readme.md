## Session 12 - 

Attendance: 0

This will close the Python Club Beginners section.

## Planned topic: Jupyter Books

Info at: https://jupyterbook.org

## Docker version

There were errors in compiling the documents from a local installation and there seems to be a compatibility issue worked on **today** (!) by developpers ([issue 715](https://github.com/executablebooks/sphinx-book-theme/issues/715).)

This is a great opportunity to use a Docker version instead that is ready to go.

I found 2 that work (See below on how to use them.)

* https://hub.docker.com/r/maciejskorski/jupyter-book-gh (with Python 3.10.10, Sphinx v5.0.2)
    * assiciated [github](https://github.com/maciejskorski/software_engineering) 
    * with [example book](https://maciejskorski.github.io/software_engineering/intro.html) online
* https://hub.docker.com/r/gnasello/jupyter-book (with Python 3.10.9, Sphinx v4.5.0)


Both have the following versions installed, obtained with command `jupyter-book --version`:

```
Jupyter Book      : 0.15.1
External ToC      : 0.3.1
MyST-Parser       : 0.18.1
MyST-NB           : 0.17.1
Sphinx Book Theme : 1.0.0
Jupyter-Cache     : 0.5.0
NbClient          : 0.5.13
```

## Build book with Docker 

Docker desktop needs to be installed on your local computer. In 201 classroom Mac-mini, use the "Intelligent hub".
On your own computer go to https://hub.docker.com/ to register (free account.)

Step 1: follow direction to create [your first book](https://jupyterbook.org/en/stable/start/your-first-book.html)

Step 2: The following command will run a docker image with a shared directory which should contain your sample book directory created with `jupyter-book` in the step above, *e.g. `mybookname`.

The following command should work on Windows, Linux and Mac systems:

```
docker run -it --rm --entrypoint "/bin/bash" -v ${PWD}:/data -w /data maciejskorski/jupyter-book-gh
```
* `it` makes the run interactive
* `rm` will close the container when exiting
* `--entrypoint` is necessary to bypass other commands
* `-v` allows sharing. `${PWD}` creates a variable for the current directory shared within the container as `/data`. `-w` sets the working directory to `/data`.

The other docker container would be activated in the same way, by just changing the image name.

Once wihtin the container, build the Jupyter book with the following command, assuming that `mybookname/` contains all the book files set-up previously.

```
jupyter-book build mybookname/
```
This will create a directory named `_build` containing the HTML version of the book. Simply open the `index.html` file within the navigate locally. This can also be uploaded to a web server for distribution (see [doc](https://jupyterbook.org/en/stable/start/publish.html).)

### Other formats

Attempt to save as PDF from the documentation ([doc](https://jupyterbook.org/en/stable/advanced/pdf.html)) did not work in both containers. 

## Conclusion

I was under the impression that Jupyter-book could be a good alternative to RStudio with Rmarkdown, but it seems quite fragile at the moment...




