# 2023-01-26-Beginners-session-03

This session will be "self-paced" from Chapter 3 from book [code materials](https://github.com/amberbiology/py4lifesci).  

Study suggestion is to use a Jupyter Notebook by starting *e.g.* a Jupyter Lab session from the Anaconda Navigator as was discussed in the first session.

Download `2013-01-26-before.ipynb` and rename it as *e.g*  `2013-01-26-after.ipynb`, or `2013-01-26-edited.ipynb` and move the file where you can open it within the Jupyter Lab session.

## NOTE:

See next session for the annotated notebook.
