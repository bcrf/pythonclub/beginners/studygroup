# Session 11
Attendance: 1

During the last session we explored some ascii/UTF-8 "byte" encoding from the lesson.

This prompted the idea to create a plot using just characters with a simple
dataset: the Beginners session attendance, shown in this table, from January 12 (Session 01) till today (March 23)

| Session #  | Attendance |
|----|----|
| 1  | 17 |
| 2  | 4  |
| 3  | 7  |
| 4  | 5  |
| 5  | 4  |
| 6  | 5  |
| 7  | 2  |
| 8  | 2  |
| 9  | 2  |
| 10 | 1  |
| 11 | 1  |

There are many ascii plot options. Here is just one:
https://github.com/glamp/bashplotlib

From which I derived this plot. Session numbers are on the X axis and Y is the value of attendance.

**See Jupyter notebook** for more details.

```
21	---------------------------------------
20	|Beginners Attendance Jan 12 - March 23|
19	---------------------------------------
18	--------------------------------------
17	| *                 |
16	|                   |
15	|                   |
14	|                   |
13	|                   |
12	|                   |
11	|                   |
10	|                   |
9	|                   |
8	|                   |
7	|     *             |
6	|                   |
5	|      *  *         |
4	|   *    *          |
3	|                   |
2	|           * **    |
1	|                ** |
0	| ------------------|
-1	--------------------------------------
```

Note: The python plot does not write a value for Y, I added it with `bash` commands after the fact:

`seq 21 -1 -1 > num17.txt` ([tip](https://www.cyberciti.biz/tips/).)

