# 2023-01-19-Beginners-session-02

This session will be "self-paced" from Chapter 2 materials.

Study suggestion is to use a Jupyter Notebook by starting *e.g.* a Jupyter Lab session from the Anaconda Navigator as was discussed in the first session.

Download the file `2013-01-19-before.ipynb` and move it to the directory used for your Jupyter Lab session.

Rename the file to *e.g.* `2013-01-19-after.ipynb` or `2013-01-19-edited.ipynb` before you open it in your session.

While reading the chapter, use the notebook to annotate what you are learning, by *e.g.* modifying the Markdown cells above the codes, or by adding new cells with the "+" sign in the notebook menu bar.

It is best to not rush things... spend the time to clearly understand the Jupyter interface, and the various ways Python operates.

If you prefer to use what is called another Integrated Development Environment (IDE) such as PyCharm, or VisualStudio, then spend the time to understand that interface.

However, Jupyter notebook are very popular and you are likley to find examples in this format.
In addition, the notebook format allows the easy exploartion of the code in smaller pieces (chunks) which may ease learning.

## Anaconda installation

* General Instructions: https://docs.anaconda.com/anaconda/install/index.html
* If you don't have Admin privilege, you can install use the "Installl for me only" (macOS) or "Just Me" (Windows) option.

> *Note*: If you use a Biochem 201 Mac Mini, the installation will be specific for you and for this Mac. Use the same Mac for every session.
