# Beginners session 01
## 2023-01-12

Number of participants present: 17

## Summary of topics

### Jupyter Lab

Demonstration of "Jupyter Lab" notebooks both on white board and on computer.   
A "notebook" consists of "Cells" that can contain Python code or text.  
The text cell can be "raw" or "Markdown", the latter allowing the easy structuring of the text with headers, font effect like *italic* or **bold**.

### Anaconda

Suggested installation: **Anaconda**, the following download link will better show the files to download based on the operating system or chip (*e.g.* new M1 chip.).  
https://www.anaconda.com/products/distribution#Downloads

### Materials and resources

We will follow the content of the book [Python for the Life Sciences – A Gentle Introduction](https://bcrf.biochem.wisc.edu/2022/11/15/python-for-the-life-sciences-a-gentle-introduction/).  
The python code is publicly available at https://github.com/amberbiology/py4lifesci

### Python  learning

We explored the ***advanced*** notion of "dot notation" which provides great power to coding in Python but makes the code difficult to understand for the uninitiated. 

The notion comes from "***Object Oriented Programming***" (OOP) with the idea that *data* and *variables* are *objects* that have specific properties based in tbe specific *nature* of the *object*. This is best understood by example such as with an *object* which *nature* is *string* e.g."some text". 

The following is a short Python script demonstrating this

```python
# Define a string:
mystring = "Some text to play with."
# The string can be printed back with:
print(str(mystring))
# The dot notation provides ready methods.
# Convert the string to all upper case
print(str.upper(mystring))
# Convert to title case:
print(str.title(mystring))
```

If the code was contained within a plain text file called `"test.py"` and run with the command `python test.py` on a terminal, the result woudl be as below and reflect the printing of the string object without modification, the string object modified by the `.upper` and `title`*methods* inherent to a *string object*.

```
Some text to play with.
SOME TEXT TO PLAY WITH.
Some Text To Play With.
```

The dot notation calls on the *methods* (akin to functions) that are built-in the object. The complete list if possible methods can be obtained with the command:

```
dir(mystring)
```

However, it should be noted that some functions may require more input or more data.


## Next sessions

I (JYS) will not be in Madison for the next 2 Thursdays (January 19 and 26) but the door door of the 201 classroom will be opened. Below is a suggestion to study to materials in a way that encourages you to annotate and observe the code supplied by the authors.


### Study: convert Python code in annotated Jupyter notebook

We learned that a Jupyter Lab is an interface with a *Jupyter notebook* with an added navigation to the file system. Either can be launched fom the "Anaconda Navigator.

*It was suggested to use a Jupyter Notebook to annotate in your own words the code material from the book, split into text or markdown cells followed by a code "chunk" in the coming sessions.*.  
All coding material is at https://github.com/amberbiology/py4lifesci

Use the code in [Chapter 2](https://github.com/amberbiology/py4lifesci/blob/master/PFTLS_Chapter_02.py) (and [Chapter 3](https://github.com/amberbiology/py4lifesci/blob/master/PFTLS_Chapter_03.py) in the next session if Chapter 2 is already finished).

Here is a flow you could follow:

* Open the book or the PDF to Chapter 2 (or 3)
* Create a Jupyter Notebook. It will automatically open in your default web browser.
	* Rename it from untitled to `CH2.ipynb` with menu within the web page"File > Rename notebook..."
* As you read in the chapter, create cells that explain the code chunk in your own words, and then add a new cell below with the python code.
	* Note: change the cell type from "Code" to "Markdown" to add your text.
	* Note: Press **Control-Return** to run the content of the cell or press the triangle "VCR Play" button in the menu bar.

