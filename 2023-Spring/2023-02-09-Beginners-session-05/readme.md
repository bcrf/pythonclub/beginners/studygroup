# Python Club - 09 February 2023

We continue our follow-up on [Chapter 3](https://github.com/amberbiology/py4lifesci/blob/master/PFTLS_Chapter_03.py)

## Note

See Jupyter Notebook `[ChatGPT_Protein_Composition.ipynb](./ChatGPT_Protein_Composition.ipynb)` for more on the Dictionary content shown below.

## Dictionary

In the last session we stopped before the examples about the dictionary structure.

The book shows 2 simple examples, one with a musician named *Nigel* and another with a gene named *p53*.

I found that this was a bit short, so I asked [**ChatGPT**](https://chat.openai.com/chat/) to help create another example using a dictionay to help find the composition of a protein.

Here is what s/he told me:

### Dictionay: definition

A dictionary in Python is a data structure that allows you to store and access data in a more organized way. It is like a physical dictionary where each word has a corresponding definition. In a Python dictionary, each item is composed of a "key" and a "value". The key is a unique identifier for the item, similar to a word in a physical dictionary, and the value is the data that is associated with that key, similar to the definition of a word.

You can use dictionaries to store many different types of data, including strings, numbers, lists, and other dictionaries. To create a dictionary, you use curly braces {} and separate keys and values with a colon : For example:

```python
dict = {'key1': 'value1', 'key2': 'value2', 'key3': 'value3'}
```

You can access the values in a dictionary using the keys. For example:

```python
print(dict['key1'])
```
his will print `'value1'`.

In biology, dictionaries can be useful for storing information about different species, for example. Each species could have a unique key, such as its scientific name, and the value could be information about its characteristics, such as its habitat, diet, and behavior.

(*Asking further for an example with a protein sequence*)

### Dictionary: use in protein composition

Here's a simple script that contains a dictionary of amino acids and uses it to analyze the composition of a protein sequence. In this case, the protein sequence is the short sequence of insulin. We'll use the one-letter code amino acids:

```python
amino_acids = {'A': 'Alanine', 'C': 'Cysteine', 'D': 'Aspartic Acid', 'E': 'Glutamic Acid',
               'F': 'Phenylalanine', 'G': 'Glycine', 'H': 'Histidine', 'I': 'Isoleucine',
               'K': 'Lysine', 'L': 'Leucine', 'M': 'Methionine', 'N': 'Asparagine',
               'P': 'Proline', 'Q': 'Glutamine', 'R': 'Arginine', 'S': 'Serine',
               'T': 'Threonine', 'V': 'Valine', 'W': 'Tryptophan', 'Y': 'Tyrosine'}

protein_sequence = "MALWMRLLPLLALLALWGPDPAAAFVNQHLCGSHLVEALYLVCGERGFFYTPKTRREAEDLQVGQVELGGGPGAGSLQPLALEGSLQKRGIVEQCCTSICSLYQLENYCN"

aa_count = {}
for aa in amino_acids:
    aa_count[aa] = protein_sequence.count(aa)

print("Amino Acid Composition of Insulin:")
for aa, count in aa_count.items():
    print(f"{amino_acids[aa]}: {count}")
```

This script first creates a dictionary of amino acids and their full names, using the one-letter code as the key. Then, it defines the protein sequence as a string. The script then uses a for loop to count the occurrences of each amino acid in the sequence, and stores the count in a new dictionary **`aa_count`**. Finally, the script prints the composition of the protein by looping through the **`aa_count`** dictionary and printing the full name of the amino acid and its count, as specified in the **`amino_acids`** dictionary.

The output of this script will be: (NOTE: these AA numbers don't match the Insulin sequence. See `ChatGPT_Protein_Composition.ipynb` for more details.)

```yaml
Amino Acid Composition of Insulin:
Alanine: 8
Cysteine: 2
Aspartic Acid: 2
Glutamic Acid: 4
Phenylalanine: 1
Glycine: 7
Histidine: 2
Isoleucine: 7
Lysine: 5
Leucine: 6
Methionine: 1
Asparagine: 4
Proline: 11
Glutamine: 4
Arginine: 6
Serine: 6
Threonine: 4
Valine: 5
Tryptophan: 0
Tyrosine: 2
```

---

### Note: Lists can be an item in a dictionary.

(Added by Chris.)

One interesting feature of dictionaries is that the items indicated by keys need not always be the same type as the key or simple objects like strings or numbers. You could, for example, nest a list inside a dictionary as an element defined by a single key:

```python
amino_acids_2 = {'A': ['Alanine','Arginine'], 'C': 'Cysteine'}

#we know this is still a dictionary by looking at its type
print(type(amino_acids_2))

#and we can use the key to call either the entire nested list
print(amino_acids_2['A'])

#or one element of the nested list 
print(amino_acids_2['A'][1])

#also remember that the subscript 1 in brackets calls the second 
#element, not the first in the set.
```


