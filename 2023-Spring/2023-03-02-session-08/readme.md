# Chapter 3 - Basic data types.

We reviewed what is presented in video for section 3 **Basic Data Types*** in  
[***Python Essential Training***](https://www.linkedin.com/learning/python-essential-training-18764650/getting-started-with-python) with the added video for list in section 4 (total video time is about 30min.) 

Relevant [Jupyter notebooks](https://github.com/LinkedInLearning/python-essential-training-4314028):

```
03_01_Ints_and_Floats.ipynb
03_02_Other_Numbers.ipynb
03_03_Booleans.ipynb
03_04_Strings.ipynb
03_05_bytes.ipynb
03_06_Challenge.ipynb
03_06_Challenge_Hints.ipynb
03_07_Solution.ipynb
04_01_lists.ipynb
```

Also from ChatGPT: How to add minuutes and seconds. This came from the problem to add all of the times from the video segments, which in the end add to about 30 minutes.

The Jupyter notebook [summing-time.ipynb](./summing-time.ipynb) contains the discussion, successful Python code and results.







Remarks at the top of the resulting files contain the information provided by ChatGPT. 
