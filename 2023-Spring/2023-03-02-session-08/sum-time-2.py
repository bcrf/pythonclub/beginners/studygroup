'''
In this example, we first parse each timestamp in the list into a 
datetime.timedelta object using the int() function to extract the minutes 
and seconds values. We then use the sum() function to add up the 
timedeltas, starting from an initial value of datetime.timedelta(). The 
resulting total time is printed to the console.
'''

import datetime

# Example list of timestamps in minutes and seconds
# timestamps = ['01:30', '03:45', '05:20', '08:10']
timestamps = ['04:29', '06:35', '06:13', '04:08', '01:32', '03:19']

# Convert each timestamp to a timedelta object
timedeltas = [datetime.timedelta(minutes=int(ts.split(':')[0]), 
seconds=int(ts.split(':')[1])) for ts in timestamps]

# Add up the timedeltas using the sum() function
total_time = sum(timedeltas, datetime.timedelta())

# Print the total time
print(total_time)

'''
Note: 
based on 
https://stackoverflow.com/questions/42487444/sum-a-list-with-timedeltas-in-python
we could also print total time from timedeltas with:
'''

# print(sum(timedeltas, datetime.timedelta()))

# Results should be the same... and they are!
